defmodule Webproject.Internale do
  @moduledoc """
  The Internale context.
  """

  import Ecto.Query, warn: false
  alias Webproject.Repo

  alias Webproject.Internale.Employee
  alias Webproject.Internalc.Company

  @doc """
  Returns the list of employees.

  ## Examples

      iex> list_employees()
      [%Employee{}, ...]

  """
  def list_employees do
    Repo.all(Employee)
  end


  def list_employees_filter(id) do
    Repo.all(from emp in Employee, join: comp in Company, on: emp.company_id==comp.id,  where: emp.company_id == ^id, select: %{employee: emp , comp: comp.company_name} )
  end

 
  def list_emp_repogenre(id) do
      Repo.all(from emp in Employee, join: comp in Company, on: emp.company_id==comp.id, group_by: [emp.company_id, emp.gender], where: emp.company_id == ^id, select:  %{gender: emp.gender, comp: emp.company_id, total: count(emp.gender) } )
  end


  def list_emp_repo_budget(id) do
    Repo.all(from emp in Employee, join: comp in Company, on: emp.company_id==comp.id, where: emp.company_id == ^id, select:  %{total_usd: sum(emp.salary), total_gbp: (sum(emp.salary)*0.74), total_eur: (sum(emp.salary)*0.88) } )
  end


  def list_emp_repocareer(id) do
    Repo.all(from emp in Employee, join: comp in Company, on: emp.company_id==comp.id, group_by: [emp.company_id, emp.career], where: emp.company_id == ^id, select:  %{carrer_repo: emp.career, comp: emp.company_id, total_repo: count(emp.career) } )
end

  @doc """
  Gets a single employee.

  Raises `Ecto.NoResultsError` if the Employee does not exist.

  ## Examples

      iex> get_employee!(123)
      %Employee{}

      iex> get_employee!(456)
      ** (Ecto.NoResultsError)

  """
  def get_employee!(id), do: Repo.get!(Employee, id)

  @doc """
  Creates a employee.

  ## Examples

      iex> create_employee(%{field: value})
      {:ok, %Employee{}}

      iex> create_employee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a employee.

  ## Examples

      iex> update_employee(employee, %{field: new_value})
      {:ok, %Employee{}}

      iex> update_employee(employee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a employee.

  ## Examples

      iex> delete_employee(employee)
      {:ok, %Employee{}}

      iex> delete_employee(employee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking employee changes.

  ## Examples

      iex> change_employee(employee)
      %Ecto.Changeset{data: %Employee{}}

  """
  def change_employee(%Employee{} = employee, attrs \\ %{}) do
    Employee.changeset(employee, attrs)
  end
end
