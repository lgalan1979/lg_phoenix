defmodule Webproject.Repo do
  use Ecto.Repo,
    otp_app: :webproject,
    adapter: Ecto.Adapters.Postgres
end
