defmodule Webproject.Internale.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "employees" do
    field :career, :string
    field :edge, :integer
    field :gender, :string
    field :name, :string
    field :salary, :decimal
    belongs_to :company, Webproject.Internalc.Company

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :edge, :gender, :career, :salary, :company_id])
    |> validate_required([:name, :edge, :gender, :career, :salary, :company_id] )
  end
end
