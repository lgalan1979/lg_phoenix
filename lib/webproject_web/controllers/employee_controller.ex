defmodule WebprojectWeb.EmployeeController do
  use WebprojectWeb, :controller

  alias Webproject.Internale
  alias Webproject.Internale.Employee

  def index(conn, _params) do
    employees = Internale.list_employees()
    render(conn, "index.html", employees: employees)
  end

  
  def showlist_emp(conn, %{"id" => id}) do
    employees = Internale.list_employees_filter( String.to_integer(id))
    render(conn, "index.html", employees: employees, id_com: id)
  end

  def showlist_emp_names(conn, %{"id" => id}) do
    employees = Internale.list_employees_filter( String.to_integer(id))
    render(conn, "showlist_emp_names.html", employees: employees, id_com: id)
  end
  
  def showlist_emp_ages(conn, %{"id" => id}) do
    employees = Internale.list_employees_filter( String.to_integer(id))
    render(conn, "showlist_emp_ages.html", employees: employees, id_com: id)
  end

  def showlist_emp_sal(conn, %{"id" => id}) do
    employees = Internale.list_employees_filter( String.to_integer(id))
    render(conn, "showlist_emp_sal.html", employees: employees, id_com: id)
  end

  def showlist_emp_genres(conn, %{"id" => id}) do
    employees = Internale.list_emp_repogenre( String.to_integer(id))
    render(conn, "showlist_emp_genres.html", employees: employees, id_com: id)
  end

  def showlist_emp_budget(conn, %{"id" => id}) do
    employees = Internale.list_emp_repo_budget( String.to_integer(id))
    render(conn, "showlist_emp_budget.html", employees: employees, id_com: id)
  end

  def showlist_emp_careers(conn, %{"id" => id}) do
    employees = Internale.list_emp_repocareer( String.to_integer(id))
    render(conn, "showlist_emp_careers.html", employees: employees, id_com: id)
  end
  
  
  def new(conn, %{"id_com" => id_com} = _params) do
    changeset = Internale.change_employee(%Employee{:company_id=>String.to_integer(id_com)})

    
    render(conn, "new.html", changeset: changeset, id_com: String.to_integer(id_com) )
  end

  def create(conn, %{"employee" => employee_params}) do
    
    %{"company_id" => company_id}=employee_params
    
      case Internale.create_employee(employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee created successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, id_com: String.to_integer(company_id))
    end
  end

  def show(conn, %{"id" => id}) do


    employee = Internale.get_employee!(id)
    render(conn, "show.html", employee: employee)
  end
  
  def edit(conn, %{"id" => id}) do
    employee = Internale.get_employee!(id)
    changeset = Internale.change_employee(employee)
    render(conn, "edit.html", employee: employee, changeset: changeset)
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    employee = Internale.get_employee!(id)

    case Internale.update_employee(employee, employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    employee = Internale.get_employee!(id)
  
    {:ok, _employee} = Internale.delete_employee(employee)
     conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :showlist_emp,employee.company_id))
  end
end
