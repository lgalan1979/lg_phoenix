defmodule WebprojectWeb.Router do
  use WebprojectWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {WebprojectWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", WebprojectWeb do
    pipe_through :browser

    get "/", PageController, :index
    resources "/companies", CompanyController
    resources "/employees", EmployeeController
    get  "/employees/list/:id/", EmployeeController, :showlist_emp
    get  "/employees/list1/:id/", EmployeeController, :showlist_emp_names
    get  "/employees/list2/:id/", EmployeeController, :showlist_emp_ages
    get  "/employees/list3/:id/", EmployeeController, :showlist_emp_sal
    get  "/employees/list4/:id/", EmployeeController, :showlist_emp_genres
    get  "/employees/list5/:id/", EmployeeController, :showlist_emp_budget
    get  "/employees/list6/:id/", EmployeeController, :showlist_emp_careers
    
  end

  # Other scopes may use custom stacks.
  # scope "/api", WebprojectWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: WebprojectWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
