defmodule Webproject.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :name, :string
      add :edge, :integer
      add :gender, :string
      add :career, :string
      add :salary, :decimal

      timestamps()
    end
  end
end
