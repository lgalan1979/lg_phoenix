defmodule Webproject.Repo.Migrations.CreateCompanies do
  use Ecto.Migration

  def change do
    create table(:companies) do
      add :company_name, :string

      timestamps()
    end
  end
end
