defmodule Webproject.InternaleTest do
  use Webproject.DataCase

  alias Webproject.Internale

  describe "employees" do
    alias Webproject.Internale.Employee

    import Webproject.InternaleFixtures

    @invalid_attrs %{career: nil, edge: nil, gender: nil, name: nil, salary: nil}

    test "list_employees/0 returns all employees" do
      employee = employee_fixture()
      assert Internale.list_employees() == [employee]
    end

    test "get_employee!/1 returns the employee with given id" do
      employee = employee_fixture()
      assert Internale.get_employee!(employee.id) == employee
    end

    test "create_employee/1 with valid data creates a employee" do
      valid_attrs = %{career: "some career", edge: 42, gender: "some gender", name: "some name", salary: "120.5"}

      assert {:ok, %Employee{} = employee} = Internale.create_employee(valid_attrs)
      assert employee.career == "some career"
      assert employee.edge == 42
      assert employee.gender == "some gender"
      assert employee.name == "some name"
      assert employee.salary == Decimal.new("120.5")
    end

    test "create_employee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Internale.create_employee(@invalid_attrs)
    end

    test "update_employee/2 with valid data updates the employee" do
      employee = employee_fixture()
      update_attrs = %{career: "some updated career", edge: 43, gender: "some updated gender", name: "some updated name", salary: "456.7"}

      assert {:ok, %Employee{} = employee} = Internale.update_employee(employee, update_attrs)
      assert employee.career == "some updated career"
      assert employee.edge == 43
      assert employee.gender == "some updated gender"
      assert employee.name == "some updated name"
      assert employee.salary == Decimal.new("456.7")
    end

    test "update_employee/2 with invalid data returns error changeset" do
      employee = employee_fixture()
      assert {:error, %Ecto.Changeset{}} = Internale.update_employee(employee, @invalid_attrs)
      assert employee == Internale.get_employee!(employee.id)
    end

    test "delete_employee/1 deletes the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{}} = Internale.delete_employee(employee)
      assert_raise Ecto.NoResultsError, fn -> Internale.get_employee!(employee.id) end
    end

    test "change_employee/1 returns a employee changeset" do
      employee = employee_fixture()
      assert %Ecto.Changeset{} = Internale.change_employee(employee)
    end
  end
end
