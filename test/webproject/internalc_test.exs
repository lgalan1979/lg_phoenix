defmodule Webproject.InternalcTest do
  use Webproject.DataCase

  alias Webproject.Internalc

  describe "companies" do
    alias Webproject.Internalc.Company

    import Webproject.InternalcFixtures

    @invalid_attrs %{company_name: nil}

    test "list_companies/0 returns all companies" do
      company = company_fixture()
      assert Internalc.list_companies() == [company]
    end

    test "get_company!/1 returns the company with given id" do
      company = company_fixture()
      assert Internalc.get_company!(company.id) == company
    end

    test "create_company/1 with valid data creates a company" do
      valid_attrs = %{company_name: "some company_name"}

      assert {:ok, %Company{} = company} = Internalc.create_company(valid_attrs)
      assert company.company_name == "some company_name"
    end

    test "create_company/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Internalc.create_company(@invalid_attrs)
    end

    test "update_company/2 with valid data updates the company" do
      company = company_fixture()
      update_attrs = %{company_name: "some updated company_name"}

      assert {:ok, %Company{} = company} = Internalc.update_company(company, update_attrs)
      assert company.company_name == "some updated company_name"
    end

    test "update_company/2 with invalid data returns error changeset" do
      company = company_fixture()
      assert {:error, %Ecto.Changeset{}} = Internalc.update_company(company, @invalid_attrs)
      assert company == Internalc.get_company!(company.id)
    end

    test "delete_company/1 deletes the company" do
      company = company_fixture()
      assert {:ok, %Company{}} = Internalc.delete_company(company)
      assert_raise Ecto.NoResultsError, fn -> Internalc.get_company!(company.id) end
    end

    test "change_company/1 returns a company changeset" do
      company = company_fixture()
      assert %Ecto.Changeset{} = Internalc.change_company(company)
    end
  end
end
