defmodule Webproject.InternaleFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Webproject.Internale` context.
  """

  @doc """
  Generate a employee.
  """
  def employee_fixture(attrs \\ %{}) do
    {:ok, employee} =
      attrs
      |> Enum.into(%{
        career: "some career",
        edge: 42,
        gender: "some gender",
        name: "some name",
        salary: "120.5"
      })
      |> Webproject.Internale.create_employee()

    employee
  end
end
