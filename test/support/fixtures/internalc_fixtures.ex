defmodule Webproject.InternalcFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Webproject.Internalc` context.
  """

  @doc """
  Generate a company.
  """
  def company_fixture(attrs \\ %{}) do
    {:ok, company} =
      attrs
      |> Enum.into(%{
        company_name: "some company_name"
      })
      |> Webproject.Internalc.create_company()

    company
  end
end
